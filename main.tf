resource "aws_iam_policy" "allow_lambda_s3" {
  name        = "s3_access_for_function_${var.lambda_name}"
  path        = "/"
  description = "IAM policy to access s3 from lambda"

  policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Effect : "Allow",
        Action : [
          "s3:GetObject"
        ],
        Resource : "${var.bucket_arn}/*"
      }
    ]
  })

  tags = {
    dev = var.environment
  }
}

resource "aws_iam_role_policy_attachment" "lambda_s3" {
  role       = var.lambda_execution_role_name
  policy_arn = aws_iam_policy.allow_lambda_s3.arn
}