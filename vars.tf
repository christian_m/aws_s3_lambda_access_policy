variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "bucket_arn" {
  description = "arn of the S3 bucket to grant access rights on"
  type        = string
}

variable "lambda_name" {
  description = "name of the lambda function"
  type        = string
}

variable "lambda_execution_role_name" {
  description = "name of the lambda execution role"
  type        = string
}